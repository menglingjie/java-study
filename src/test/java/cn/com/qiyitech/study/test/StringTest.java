package cn.com.qiyitech.study.test;


import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class StringTest {
    @Test
    public void testReplaceCaseInsensitive(){
        String expectString = "1234567890abcdef";
        String target = "Bearer " + expectString;
        String ret = target.replaceAll("(?i)bearer", "").trim();
        assertEquals(expectString, ret);
    }
    @Test
    public void testReplaceCaseSensitive(){
        String expectString = "1234567890abcdef";
        String target = "Bearer " + expectString;
        String ret = target.replaceAll("bearer", "").trim();
        assertNotEquals(expectString, ret);
        assertEquals(target, ret);
    }
}
